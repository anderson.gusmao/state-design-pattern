//
//  LaundryWasherMachine.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

class LaundryWasherMachine: LaundryWasherMachinable {

    private var state: LaundryWasherState

    var currentState: LaundryWasherState { state }

    init(initialState: LaundryWasherState) {
        state = initialState
    }

    func requestTurnOn() throws {
        guard currentState.canTurnOn else { throw OperationError.notAllowed }
        state = TurnedOnState()

        //TODO: something here....
    }

    func requestTurnOff() throws {
        guard currentState.canTurnOff else { throw OperationError.notAllowed }
        state = TurnedOffState()

        //TODO: something here....
    }

    func requestRinse() throws {
        guard currentState.canRinse else { throw OperationError.notAllowed }
        state = RinseState()

        //TODO: something here....
    }

    func requestSpin() throws {
        guard currentState.canSpin else { throw OperationError.notAllowed }
        state = SpinState()

        //TODO: something here....
    }

    func requestDry() throws {
        guard currentState.canDry else { throw OperationError.notAllowed }
        state = DryState()

        //TODO: something here....
    }
}
