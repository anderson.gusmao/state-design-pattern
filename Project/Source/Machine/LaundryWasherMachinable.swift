//
//  LaundryWasherMachinable.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

protocol LaundryWasherMachinable {

    var currentState: LaundryWasherState { get }

    func requestTurnOn() throws
    func requestTurnOff() throws
    func requestRinse() throws
    func requestSpin() throws
    func requestDry() throws
}
