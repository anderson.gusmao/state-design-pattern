//
//  SpinState.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

struct SpinState: LaundryWasherState {

    var canTurnOn: Bool { false }
    var canTurnOff: Bool { true }
    var canRinse: Bool { false }
    var canSpin: Bool { false }
    var canDry: Bool { true }
}
