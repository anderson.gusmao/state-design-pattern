//
//  DryState.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

struct DryState: LaundryWasherState {

    var canTurnOn: Bool { false }
    var canTurnOff: Bool { true }
    var canRinse: Bool { false }
    var canSpin: Bool { false }
    var canDry: Bool { false }
}
