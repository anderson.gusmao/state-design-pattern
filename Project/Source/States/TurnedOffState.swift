//
//  TurnedOffState.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

struct TurnedOffState: LaundryWasherState {

    var canTurnOn: Bool { true }
    var canTurnOff: Bool { false }
    var canRinse: Bool { false }
    var canSpin: Bool { false }
    var canDry: Bool { false }
}
