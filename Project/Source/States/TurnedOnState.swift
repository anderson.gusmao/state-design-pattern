//
//  TurnedOnState.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

struct TurnedOnState: LaundryWasherState {

    var canTurnOn: Bool { false }
    var canTurnOff: Bool { true }
    var canRinse: Bool { true }
    var canSpin: Bool { false }
    var canDry: Bool { true }
}
