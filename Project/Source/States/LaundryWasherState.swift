//
//  LaundryWasherState.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

protocol LaundryWasherState {

    var canTurnOn: Bool { get }
    var canTurnOff: Bool { get }
    var canRinse: Bool { get }
    var canSpin: Bool { get }
    var canDry: Bool { get }
}
