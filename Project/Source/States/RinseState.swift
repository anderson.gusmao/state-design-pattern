//
//  RinseState.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

struct RinseState: LaundryWasherState {

    var canTurnOn: Bool { false }
    var canTurnOff: Bool { true }
    var canRinse: Bool { false }
    var canSpin: Bool { true }
    var canDry: Bool { false }
}

