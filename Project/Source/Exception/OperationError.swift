//
//  OperationError.swift
//  Bird
//
//  Created by developer on 16/11/21.
//

import Foundation

enum OperationError: Error {
    case notAllowed
}
