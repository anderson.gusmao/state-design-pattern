//
//  UnitTests.swift
//  UnitTest
//
//  Created by developer on 16/11/21.
//

import XCTest
@testable import BashWash

class UnitTests: XCTestCase {

    func testTurnedOffTransition() throws {
        let machine = LaundryWasherMachine(initialState: TurnedOffState())

        XCTAssertThrowsError(try machine.requestTurnOff())
        XCTAssertThrowsError(try machine.requestRinse())
        XCTAssertThrowsError(try machine.requestSpin())
        XCTAssertThrowsError(try machine.requestDry())
        XCTAssertThrowsError(try machine.requestTurnOff())

        try! machine.requestTurnOn()
        XCTAssertTrue(machine.currentState is TurnedOnState)
        XCTAssertThrowsError(try machine.requestTurnOn())
    }

    func testTurnedOnTransition() throws {
        let machine = LaundryWasherMachine(initialState: TurnedOnState())

        XCTAssertThrowsError(try machine.requestTurnOn())
        XCTAssertThrowsError(try machine.requestSpin())

        try! machine.requestRinse()
        XCTAssertTrue(machine.currentState is RinseState)
        XCTAssertThrowsError(try machine.requestRinse())
        try! machine.requestTurnOff()

        try! machine.requestTurnOn()
        try! machine.requestDry()
    }

    func testRinseTransition() throws {
        let machine = LaundryWasherMachine(initialState: RinseState())

        XCTAssertThrowsError(try machine.requestTurnOn())
        XCTAssertThrowsError(try machine.requestRinse())
        XCTAssertThrowsError(try machine.requestDry())

        try! machine.requestSpin()
        XCTAssertTrue(machine.currentState is SpinState)
        XCTAssertThrowsError(try machine.requestSpin())
        try! machine.requestTurnOff()

        XCTAssertThrowsError(try machine.requestSpin())
    }

    func testSpinTransition() throws {
        let machine = LaundryWasherMachine(initialState: SpinState())

        XCTAssertThrowsError(try machine.requestTurnOn())
        XCTAssertThrowsError(try machine.requestRinse())
        XCTAssertThrowsError(try machine.requestSpin())

        try! machine.requestDry()
        XCTAssertTrue(machine.currentState is DryState)
        XCTAssertThrowsError(try machine.requestSpin())
        XCTAssertThrowsError(try machine.requestTurnOn())
        XCTAssertThrowsError(try machine.requestRinse())
        try! machine.requestTurnOff()
    }

    func testDryTransition() throws {
        let machine = LaundryWasherMachine(initialState: DryState())

        XCTAssertThrowsError(try machine.requestTurnOn())
        XCTAssertThrowsError(try machine.requestRinse())
        XCTAssertThrowsError(try machine.requestSpin())
        XCTAssertThrowsError(try machine.requestDry())

        try! machine.requestTurnOff()
        XCTAssertTrue(machine.currentState is TurnedOffState)
    }

    func testOnOffTransition() throws {
        let machine = LaundryWasherMachine(initialState: TurnedOffState())

        try! machine.requestTurnOn()
        try! machine.requestTurnOff()
    }

    func testallTransitions() throws {
        let machine = LaundryWasherMachine(initialState: TurnedOffState())

        try! machine.requestTurnOn()
        XCTAssertTrue(machine.currentState is TurnedOnState)

        try! machine.requestRinse()
        XCTAssertTrue(machine.currentState is RinseState)

        try! machine.requestSpin()
        XCTAssertTrue(machine.currentState is SpinState)

        try! machine.requestDry()
        XCTAssertTrue(machine.currentState is DryState)

        try! machine.requestTurnOff()
        XCTAssertTrue(machine.currentState is TurnedOffState)
    }
}
