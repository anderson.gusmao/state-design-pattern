![](https://badgen.net/badge/Unit%20tests/100%25/green) ![](https://badgen.net/badge/Language/swift/purple)
# What is this?
This is a demonstration of how to implement the state design pattern. It was implemented using the swift language with Xcode IDE, but of course, this simple example can be adapted to any kind of programming language.

# About state design pattern

The state pattern is a behavioral software design pattern that allows an object to change its behavior when its internal state changes. This pattern is close to the concept of finite state machines. 

The state pattern is used in computer programming to encapsulate varying behavior for the same object based on its internal state. This can be a cleaner way for an object to change its runtime behavior without resorting to conditional statements, and thus improve maintainability.

# Laundry washer example
As a state design pattern use case, this project presents the states of a washing machine. Off, on, rinse, spin, and dry states are displayed. Take a look in the picture below.

![](https://gitlab.com/anderson.gusmao/state-design-pattern/-/raw/main/Resources/LaundryWasherStates.png)

## Possible states and capabilities
As proposed by the diagram logic:

1. Once the machine is turned off, it can only be turned on.
2. With the machine turned on, it can be turned off, triggering the rinsing or drying process.
3. When the machine is in the process of rinsing, it can be turned off or go to the spin process.
4. When the machine is in the spin process, it can be turned off or go into the drying process.
5. And finally, when the machine is in the drying process, it can only be turned off.

# Class diagram

![](https://gitlab.com/anderson.gusmao/state-design-pattern/-/raw/main/Resources/ClassDiagram.png)
